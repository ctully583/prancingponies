apiVersion: v1
kind: BuildConfig
metadata:
  name: pp-autotrader-pipeline
spec:
  triggers:
    - type: Generic
      generic:
        secret: wQEutbB1gF98uGrFk7Pg
  strategy:
    jenkinsPipelineStrategy:
      env:
        - name: ANGULARCLIVERSION
          description: Angular version to compile frontend
          value: 7.0.3
        - name: BACKENDGITREPO
          description: Backend git repository
          value: 'https://bitbucket.org/ctully583/prancingponies.git'
        - name: FRONTENDGITREPO
          description: Frontend git repository
          value: 'https://bitbucket.org/vermilion38/fe-prancing-ponies.git'
        - name: TEAMNAME
          description: Team name for docker images
          value: 'pp'
        - name: APPNAME
          description: Application name for docker images and builds
          value: 'prancingponies'
        - name: VERSIONBASE
          description: Base for version number
          value: '0.0'
      jenkinsfile: |-
        pipeline {
          agent { label "maven" }
          stages {
            stage("Clone Backend Source") {
              steps {
                dir('spring-app') {
                  git(
                    url: "${BACKENDGITREPO}",
                    branch: "master"
                  )
                }
              }
            }

            stage("Build JAR") {
              steps {
                dir('spring-app') {
                  sh 'mvn clean package'
                }
              }
              post {
                success {
                  dir('spring-app') {
                    junit 'target/surefire-reports/**/*.xml'
                    archiveArtifacts artifacts: 'target/site/jacoco/**/*', fingerprint: true
                  }
                }
              }

            }

            stage("Build App Image") {
              steps {
                dir('spring-app') {
                  sh "oc new-build --strategy docker --binary --name $APPNAME-app || echo \"Build already exists\""
                  sh "oc patch bc/$APPNAME-app --patch \'{\"spec\":{\"output\":{\"to\":{\"kind\":\"DockerImage\", \"name\":\"dockerreg.conygre.com:5000/$TEAMNAME/$APPNAME-app:$VERSIONBASE.${env.BUILD_NUMBER}\"}}}}\'"
                  sh 'cp Dockerfile-app Dockerfile'
                  sh "oc start-build $APPNAME-app --from-dir . --follow && rm Dockerfile"
                }
              }
            }

            stage("Build Database Image") {
              steps {
                dir('spring-app') {
                  sh "oc new-build --strategy docker --binary --name $APPNAME-mysql || echo \"Build already exists\""
                  sh "oc patch bc/$APPNAME-mysql --patch \'{\"spec\":{\"output\":{\"to\":{\"kind\":\"DockerImage\", \"name\":\"dockerreg.conygre.com:5000/$TEAMNAME/$APPNAME-mysql:$VERSIONBASE.${env.BUILD_NUMBER}\"}}}}\'"
                  sh 'cp Dockerfile-mysql Dockerfile'
                  sh 'oc start-build $APPNAME-mysql --from-dir . --follow && rm Dockerfile'
                }
              }
            }

            stage ("Build Front End") {
              agent {label 'nodejs'}
              stages {
                stage("Clone Frontend Source") {
                  steps {
                    dir('frontend') {
                      git(
                        url: "${FRONTENDGITREPO}",
                        branch: "master"
                      )
                    }
                  }
                }
                stage("Install Angular") {
                  steps {
                    dir('frontend') {
                      sh "npm install -g @angular/cli@${ANGULARCLIVERSION}"
                    }
                  }
                }
                stage("Angular build") {
                  steps {
                    dir('frontend') {
                      sh 'npm install'
                      sh 'ng build --prod=true'
                    }
                  }
                }
                stage("Build Frontend Image") {
                  steps {
                    dir('frontend') {
                      sh 'oc new-build --strategy docker --binary --name $APPNAME-fe || echo "Build already exists"'
                      sh "oc patch bc/$APPNAME-fe --patch \'{\"spec\":{\"output\":{\"to\":{\"kind\":\"DockerImage\", \"name\":\"dockerreg.conygre.com:5000/$TEAMNAME/$APPNAME-fe:$VERSIONBASE.${env.BUILD_NUMBER}\"}}}}\'"
                      sh "oc start-build $APPNAME-fe --from-dir . --follow --wait=true || (echo \"BUILD FAILED\" && exit 1)"
                    }
                  }
                }
              }
            }

            stage("Delete old apps") {
              steps {
                sh "oc delete all -l app=$APPNAME-app"
                sh "oc delete all -l app=$APPNAME-mysql"
                sh "oc delete all -l app=$APPNAME-fe"
              }
            }

            stage("Deploy new images") {
              steps {
                sh "oc new-app --insecure-registry -e MYSQL_DATABASE=traderdb -e MYSQL_ROOT_PASSWORD=root --docker-image dockerreg.conygre.com:5000/$TEAMNAME/$APPNAME-mysql:$VERSIONBASE.${env.BUILD_NUMBER}"
                sh "oc new-app --insecure-registry -e DB_HOST=$APPNAME-mysql -e DB_USER=root -e DB_PASS=root -e SERVER_PORT=8081 -e MQ_HOST=mqbroker -e MQ_PORT=61616 --docker-image dockerreg.conygre.com:5000/$TEAMNAME/$APPNAME-app:$VERSIONBASE.${env.BUILD_NUMBER}"
                sh "oc new-app --insecure-registry --docker-image dockerreg.conygre.com:5000/$TEAMNAME/$APPNAME-fe:$VERSIONBASE.${env.BUILD_NUMBER}"
              }
            }

            stage("Expose Apps") {
              steps {
                sh "oc expose svc/$APPNAME-app"
                sh "oc expose --hostname=${TEAMNAME}.dev4.conygre.com --name api-$APPNAME-app --path=/trade svc/$APPNAME-app"
                sh "oc expose --hostname=${TEAMNAME}.dev4.conygre.com --path=/ --port=8080 svc/$APPNAME-fe"
              }
            }
          }
        }
      type: JenkinsPipeline

