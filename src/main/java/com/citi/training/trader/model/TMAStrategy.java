package com.citi.training.trader.model;

import java.util.Date;

public class TMAStrategy {

    private int id;
    private Stock stock;
    private int size;
    private double longAverage;
    private double shortAverage;
    private Date longPeriodStart;
    private Date longPeriodEnd;
    private Date shortPeriodStart;
    private Date shortPeriodEnd;
    private double profitLossPer;
    private Date stopped;
    
    public TMAStrategy(Stock stock, int size, Date longPeriodStart, Date longPeriodEnd, 
    		Date shortPeriodStart, Date shortPeriodEnd) {
    	this(-1, stock, size, 0, 0, longPeriodStart, longPeriodEnd, shortPeriodStart, 
    			shortPeriodEnd, 0, null);
    }

	public TMAStrategy(int id, Stock stock, int size, double longAverage, 
			double shortAverage, Date longPeriodStart, Date longPeriodEnd, 
			Date shortPeriodStart, Date shortPeriodEnd, double profitLossPer, 
			Date stopped) {
		this.id = id;
		this.stock = stock;
		this.size = size;
		this.longAverage = longAverage;
		this.shortAverage = shortAverage;
		this.longPeriodStart = longPeriodStart;
		this.longPeriodEnd = longPeriodEnd;
		this.shortPeriodStart = shortPeriodStart;
		this.shortPeriodEnd = shortPeriodEnd;
		this.profitLossPer = profitLossPer;
		this.stopped = stopped;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Stock getStock() {
		return stock;
	}

	public void setStock(Stock stock) {
		this.stock = stock;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public double getLongAverage() {
		return longAverage;
	}

	public void setLongAverage(double longAverage) {
		this.longAverage = longAverage;
	}

	public double getShortAverage() {
		return shortAverage;
	}

	public void setShortAverage(double shortAverage) {
		this.shortAverage = shortAverage;
	}

	public Date getLongPeriodStart() {
		return longPeriodStart;
	}

	public void setLongPeriodStart(Date longPeriodStart) {
		this.longPeriodStart = longPeriodStart;
	}
	
	public Date getLongPeriodEnd() {
		return longPeriodEnd;
	}

	public void setLongPeriodEnd(Date longPeriodEnd) {
		this.longPeriodEnd = longPeriodEnd;
	}

	public Date getShortPeriodStart() {
		return shortPeriodStart;
	}

	public void setShortPeriodStart(Date shortPeriodStart) {
		this.shortPeriodStart = shortPeriodStart;
	}

	public Date getShortPeriodEnd() {
		return shortPeriodEnd;
	}

	public void setShortPeriodEnd(Date shortPeriodEnd) {
		this.shortPeriodEnd = shortPeriodEnd;
	}

	public double getProfitLossPer() {
		return profitLossPer;
	}

	public void setProfitLossPer(double profitLossPer) {
		this.profitLossPer = profitLossPer;
	}

	public Date getStopped() {
		return stopped;
	}

	public void setStopped(Date stopped) {
		this.stopped = stopped;
	}

	@Override
	public String toString() {
		return "TMAStrategy [id=" + id + ", stock=" + stock + ", size=" + size + ", longAverage=" + longAverage
				+ ", shortAverage=" + shortAverage + ", longPeriodStart=" + longPeriodStart + ", longPeriodEnd="
				+ longPeriodEnd + ", shortPeriodStart=" + shortPeriodStart + ", shortPeriodEnd=" + shortPeriodEnd
				+ ", profitLossPer=" + profitLossPer + ", stopped=" + stopped + "]";
	}
}
