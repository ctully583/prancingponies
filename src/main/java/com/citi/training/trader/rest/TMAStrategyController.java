package com.citi.training.trader.rest;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.trader.service.TradeService;
import com.citi.training.trader.service.TMAStrategyService;
import com.citi.training.trader.strategy.TMAStrategyAlgorithm;
import com.citi.training.trader.dao.TMAStrategyDao;
import com.citi.training.trader.model.Stock;
import com.citi.training.trader.model.TMAStrategy;
import com.citi.training.trader.model.Trade;

/**
 * REST Controller for {@link com.citi.training.trader.model.TMAStrategy} resource.
 *
 */
@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("${com.citi.training.trader.rest.trade-base-path:/tmastrategy}")

public class TMAStrategyController {
	
	private static final Logger logger =
            LoggerFactory.getLogger(TMAStrategyController.class);
	
	@Autowired
    private TradeService TradeService;
	
	@Autowired
    private TMAStrategyService TMAStrategyService;
    
    @Autowired
    private TMAStrategyAlgorithm tmaStrategyAlgorithm;
    
    @Autowired
    private TMAStrategyDao tmaStrategyDao;
	

}
