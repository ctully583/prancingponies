package com.citi.training.trader.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.TMAStrategyDao;
import com.citi.training.trader.model.TMAStrategy;

@Component
public class TMAStrategyService {

	@Autowired
	private TMAStrategyDao tmaStrategyDao;
	
	public List<TMAStrategy> findAll() {
		return tmaStrategyDao.findAll();
	}
	
	public int save(TMAStrategy tmaStrategy) {
		return tmaStrategyDao.save(tmaStrategy);
	}
}
