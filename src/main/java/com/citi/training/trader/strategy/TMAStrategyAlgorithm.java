package com.citi.training.trader.strategy;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.citi.training.trader.exceptions.PriceNotFoundException;
import com.citi.training.trader.exceptions.TradeNotFoundException;
import com.citi.training.trader.messaging.TradeSender;
import com.citi.training.trader.model.Price;
import com.citi.training.trader.model.TMAStrategy;
import com.citi.training.trader.model.Trade;
import com.citi.training.trader.model.Trade.TradeType;
import com.citi.training.trader.service.PriceService;
import com.citi.training.trader.service.TMAStrategyService;
import com.citi.training.trader.service.TradeService;

@Profile("!no-scheduled")
@Component
public class TMAStrategyAlgorithm implements StrategyAlgorithm {

	private static final Logger logger =
            LoggerFactory.getLogger(TMAStrategyAlgorithm.class);

	@Autowired
    private TradeSender tradeSender;
	
	@Autowired
    private TradeService tradeService;
	
	@Autowired
	private PriceService priceService;

	@Autowired
	private TMAStrategyService tmaStrategyService;
	
	@Scheduled(fixedRateString = "${simple.strategy.refresh.rate_ms:5000}")
	public void run() {
		for (TMAStrategy tmaStrategy : tmaStrategyService.findAll()) {
			
			List<Price> prices = null;
            try {
                prices = priceService.findAll(tmaStrategy.getStock());
            } catch(PriceNotFoundException ex) {
                logger.debug("No Prices for strategy id: [" + tmaStrategy.getId() + "]");
            }
            List<Price> longPrices = new ArrayList<Price>();
            for (Price price : prices) {
            	if (price.getRecordedAt().after(tmaStrategy.getLongPeriodStart()) && 
            			price.getRecordedAt().before(tmaStrategy.getLongPeriodEnd())) {
            		longPrices.add(price);
            	}
            }
            List<Price> shortPrices = new ArrayList<Price>();
            for (Price price : prices) {
            	logger.debug("Price Recorded at: " + price.getRecordedAt());
            	if (price.getRecordedAt().after(tmaStrategy.getShortPeriodStart()) && 
            			price.getRecordedAt().before(tmaStrategy.getShortPeriodEnd())) {
            		shortPrices.add(price);
            	}
            }
            double longPriceTotal = 0;
            double longPriceAvg = 0;
            for (Price price : longPrices) {
            	longPriceTotal += price.getPrice();
            }
            longPriceAvg = longPriceTotal /longPrices.size();
            
            double shortPriceTotal = 0;
            double shortPriceAvg = 0;
            for (Price price : shortPrices) {
            	shortPriceTotal += price.getPrice();
            }
            shortPriceAvg = shortPriceTotal / shortPrices.size();
            double price = 0;
            if (shortPriceAvg > longPriceAvg) {
            	if (tmaStrategy.getShortAverage() < tmaStrategy.getLongAverage()) {
            		//time to buy
            		price = makeTrade(tmaStrategy, TradeType.BUY);
            	}
            }
            else {
				if (tmaStrategy.getShortAverage() > tmaStrategy.getLongAverage()) {
					//time to sell
					price = makeTrade(tmaStrategy, TradeType.SELL);
				}
			}
            tmaStrategy.setLongAverage(longPriceAvg);
            tmaStrategy.setShortAverage(shortPriceAvg);
            
            Calendar calendarLong = Calendar.getInstance();
        	calendarLong.setTime(tmaStrategy.getLongPeriodEnd());
        	logger.debug("Long Period End: " + tmaStrategy.getLongPeriodEnd());
        	calendarLong.add(Calendar.MINUTE, 5);
        	Date longPeriodEnd = calendarLong.getTime();
        	tmaStrategy.setLongPeriodStart(tmaStrategy.getLongPeriodEnd());
            tmaStrategy.setLongPeriodEnd(longPeriodEnd);
            
            Calendar calendarShort = Calendar.getInstance();
        	calendarShort.setTime(tmaStrategy.getShortPeriodEnd());
        	calendarShort.add(Calendar.MINUTE, 1);
        	Date shortPeriodEnd = calendarShort.getTime();
        	tmaStrategy.setShortPeriodStart(tmaStrategy.getShortPeriodEnd());
            tmaStrategy.setShortPeriodEnd(shortPeriodEnd);
            tmaStrategy.setProfitLossPer(0);
            tmaStrategy.setStopped(new Date());
            logger.debug(tmaStrategy.toString());
            tmaStrategyService.save(tmaStrategy);
            Trade previousTrade = null;
            try {
            	previousTrade = tradeService.findLatestByStrategyId(tmaStrategy.getId());
            }
            catch (TradeNotFoundException e) {
            }
            if (previousTrade != null) {
            	double profitLoss = price / previousTrade.getPrice() * 100;
                tmaStrategy.setProfitLossPer(profitLoss);
                tmaStrategyService.save(tmaStrategy);
            }
            
            
            if (tmaStrategy.getProfitLossPer() > 1 || tmaStrategy.getProfitLossPer() < 1) {
            	tmaStrategy.setStopped(new Date());
            	continue;
            }
		}
	}
	
	private double makeTrade(TMAStrategy strategy, Trade.TradeType tradeType) {
        Price currentPrice = priceService.findLatest(strategy.getStock(), 1).get(0);
        tradeSender.sendTrade(new Trade(currentPrice.getPrice(),
                                        strategy.getSize(), tradeType,
                                        strategy));
        return currentPrice.getPrice();
    }
}
