package com.citi.training.trader.dao.mysql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.TMAStrategyDao;
import com.citi.training.trader.exceptions.TMAStrategyNotFoundException;
import com.citi.training.trader.model.Stock;
import com.citi.training.trader.model.TMAStrategy;

/**
 * JDBC MySQL DAO implementation for tma_strategy table.
 *
 */
@Component
public class MysqlTMAStrategyDao implements TMAStrategyDao {

	private static final Logger logger =
            LoggerFactory.getLogger(MysqlTMAStrategyDao.class);
	
	private static String FIND_ALL_SQL = "select tma_strategy.id as strategy_id, stock.id as stock_id, stock.ticker, " +
            "size, long_average, short_average, long_period_start, long_period_end, short_period_start, short_period_end, profit_loss_per, stopped " +
            "from tma_strategy left join stock on stock.id = tma_strategy.stock_id";

	private static String FIND_SQL = FIND_ALL_SQL + " where tma_strategy.id = ?";

	private static String INSERT_SQL = "INSERT INTO tma_strategy (stock_id, size, long_average, short_average, " +
          "long_period_start, long_period_end, short_period_start, short_period_end, profit_loss_per, stopped) " +
          "values (:stock_id, :size, :long_average, :short_average, "+ 
          ":long_period_start, :long_period_end, :short_period_start, :short_period_end, :profit_loss_per, :stopped)";

	private static String UPDATE_SQL = "UPDATE tma_strategy set stock_id=:stock_id, size=:size, " +
          "long_average=:long_average, short_average=:short_average, " +
          "long_period_start=:long_period_start, long_period_end=:long_period_end, "
          + "short_period_start=:short_period_start, "
          + "short_period_end=:short_period_end, profit_loss_per=:profit_loss_per, "
          + "stopped=:stopped where id=:id";

	@Autowired
	private JdbcTemplate tpl;

	@Autowired
	NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	@Override
	public List<TMAStrategy> findAll() {
		logger.debug("findAll SQL: [" + FIND_ALL_SQL + "]");
        return tpl.query(FIND_ALL_SQL,
                         new TMAStrategyMapper());
	}

	@Override
	public TMAStrategy findById(int id) {
		logger.debug("findBydId(" + id + ") SQL: [" + FIND_SQL + "]");
        List<TMAStrategy> tmaStrategies = this.tpl.query(FIND_SQL,
                new Object[]{id},
                new TMAStrategyMapper()
        );
        if(tmaStrategies.size() <= 0) {
            String warnMsg = "Requested TMA Strategy not found, id: " + id;
            logger.warn(warnMsg);
            throw new TMAStrategyNotFoundException(warnMsg);
        }
        if(tmaStrategies.size() > 1) {
            logger.warn("Found more than one TMA Strategy with id: " + id);
        }
        return tmaStrategies.get(0);
	}

	@Override
	public int save(TMAStrategy tmaStrategy) {
		MapSqlParameterSource namedParameters = new MapSqlParameterSource();

        namedParameters.addValue("stock_id", tmaStrategy.getStock().getId());
        namedParameters.addValue("size", tmaStrategy.getSize());
        namedParameters.addValue("long_average", tmaStrategy.getLongAverage());
        namedParameters.addValue("short_average", tmaStrategy.getShortAverage());
        namedParameters.addValue("long_period_start", tmaStrategy.getLongPeriodStart());
        namedParameters.addValue("long_period_end", tmaStrategy.getLongPeriodEnd());
        namedParameters.addValue("short_period_start", tmaStrategy.getShortPeriodStart());
        namedParameters.addValue("short_period_end", tmaStrategy.getShortPeriodEnd());
        namedParameters.addValue("profit_loss_per", tmaStrategy.getProfitLossPer());
        namedParameters.addValue("stopped", tmaStrategy.getStopped());

        if(tmaStrategy.getId() < 0) {
            logger.debug("Inserting tmaStrategy: " + tmaStrategy);

            KeyHolder keyHolder = new GeneratedKeyHolder();

            namedParameterJdbcTemplate.update(INSERT_SQL, namedParameters, keyHolder);
            tmaStrategy.setId(keyHolder.getKey().intValue());
        } else {
            logger.debug("Updating tmaStrategy: " + tmaStrategy);
            namedParameters.addValue("id", tmaStrategy.getId());
            namedParameterJdbcTemplate.update(UPDATE_SQL, namedParameters);
        }

        logger.debug("Saved trade: " + tmaStrategy);
        return tmaStrategy.getId();
	}

	/**
     * private internal class to map database rows to SimpleStrategy objects.
     *
     */
    private static final class TMAStrategyMapper implements RowMapper<TMAStrategy> {
        public TMAStrategy mapRow(ResultSet rs, int rowNum) throws SQLException {
            logger.debug("Mapping tma_strategy result set row num [" + rowNum + "], id : [" +
                         rs.getInt("strategy_id") + "]");

            Timestamp timestampLPS = rs.getTimestamp("long_period_start");
            if (timestampLPS == null) {
            	logger.error("Issue with Date Timestamp conversion");
            }
            Date dateLPS = new Date(timestampLPS.getTime());
            
            Timestamp timestampLPE = rs.getTimestamp("long_period_end");
            if (timestampLPE == null) {
            	logger.error("Issue with Date Timestamp conversion");
            }
            Date dateLPE = new Date(timestampLPE.getTime());
            
            Timestamp timestampSPS = rs.getTimestamp("short_period_start");
            if (timestampSPS == null) {
            	logger.error("Issue with Date Timestamp conversion");
            }
            Date dateSPS = new Date(timestampSPS.getTime());
            
            Timestamp timestampSPE = rs.getTimestamp("short_period_end");
            if (timestampSPE == null) {
            	logger.error("Issue with Date Timestamp conversion");
            }
            Date dateSPE = new Date(timestampSPE.getTime());
            
            return new TMAStrategy(rs.getInt("strategy_id"),
                             new Stock(rs.getInt("stock_id"),
                                       rs.getString("stock.ticker")),
                             rs.getInt("size"),
                             rs.getDouble("long_average"),
                             rs.getDouble("short_average"),
                             dateLPS,
                             dateLPE,
                             dateSPS,
                             dateSPE,
                             rs.getDouble("profit_loss_per"),
                             rs.getDate("stopped"));
        }
    }
}
