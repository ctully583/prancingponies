package com.citi.training.trader.dao;

import java.util.List;

import com.citi.training.trader.model.TMAStrategy;

public interface TMAStrategyDao {

	List<TMAStrategy> findAll();
	
	TMAStrategy findById(int id);
	
	int save(TMAStrategy tmaStrategy);
}
