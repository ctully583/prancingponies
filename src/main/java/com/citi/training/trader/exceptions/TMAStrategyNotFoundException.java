package com.citi.training.trader.exceptions;

@SuppressWarnings("serial")
public class TMAStrategyNotFoundException extends RuntimeException {

	public TMAStrategyNotFoundException(String message) {
		super(message);
	}
}
