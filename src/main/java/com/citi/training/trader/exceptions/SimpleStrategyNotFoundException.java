package com.citi.training.trader.exceptions;

/**
 * An exception to be thrown by {@link com.citi.trading.dao.SimpleStrategyDao} implementations
 * when a requested simple strategy is not found.
 */
@SuppressWarnings("serial")
public class SimpleStrategyNotFoundException extends RuntimeException {

	public SimpleStrategyNotFoundException(String message) {
		super(message);
	}
}
