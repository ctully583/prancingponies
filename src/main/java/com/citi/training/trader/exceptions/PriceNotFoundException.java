package com.citi.training.trader.exceptions;

@SuppressWarnings("serial")
public class PriceNotFoundException extends RuntimeException {

	public PriceNotFoundException(String message) {
		super(message);
	}
}
