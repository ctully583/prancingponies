CREATE TABLE IF NOT EXISTS stock
(
  `id` INT NOT NULL AUTO_INCREMENT,
  `ticker` VARCHAR(8) NOT NULL,
  UNIQUE(`ticker`),
  PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS tma_strategy
(
	`id` INT NOT NULL AUTO_INCREMENT,
	`stock_id` INT NOT NULL,
	`size` INT NOT NULL,
	`long_average` DOUBLE,
	`short_average` DOUBLE,
	`long_period_start` DATETIME NOT NULL,
	`long_period_end` DATETIME NOT NULL,
	`short_period_start` DATETIME NOT NULL,
	`short_period_end` DATETIME NOT NULL,
	`profit_loss_per` DOUBLE NOT NULL,
	`stopped` DATETIME,
	CONSTRAINT `tmastrategy_stock_foreign_key`
    FOREIGN KEY (`stock_id`) REFERENCES stock (`id`),
  	PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `trade`
(
  `id` INT NOT NULL AUTO_INCREMENT,
  `strategy_id` INT NOT NULL,
  `price` DOUBLE NOT NULL,
  `size` INT NOT NULL,
  `buy` BOOLEAN NOT NULL,
  `state` VARCHAR(20),
  `last_state_change` DATETIME NOT NULL,
  `accounted_for` INT DEFAULT 0,
  `created` DATETIME NOT NULL,
    CONSTRAINT `trade_tma_strategy_foreign_key`
    FOREIGN KEY (`strategy_id`) REFERENCES tma_strategy (`id`),
  PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS price
(
  `id` INT NOT NULL AUTO_INCREMENT,
  `stock_id` INT NOT NULL,
  `price` DOUBLE,
  `recorded_at` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `price_stock`
    FOREIGN KEY (`stock_id`) REFERENCES stock (`id`)
);
