package com.citi.training.trader.rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trader.model.Stock;
import com.citi.training.trader.model.TMAStrategy;
import com.citi.training.trader.model.Trade;
import com.citi.training.trader.model.Trade.TradeType;

/**
 * Integration test for Trade REST Interface.
 *
 * Makes HTTP requests to {@link com.citi.training.trader.rest.TradeController}.
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("h2")
public class TradeControllerIT {

	private static final Logger logger =
            LoggerFactory.getLogger(TradeControllerIT.class);

	@Autowired
	private TestRestTemplate restTemplate;
	
	private TMAStrategy tmaStrategy;
	private Stock stock;

	@Value("${com.citi.trading.trader.rest.trade-base-path:/trade}")
	private String tradeBasePath;
	
	@Before
	public void setUp() throws Exception {
		stock = new Stock(1, "APPL");
	}

//	@Test
//	@Transactional
//	public void findAll_returnsList() {
//		tmaStrategy = new TMAStrategy(stock, 20, new Date(), new Date(), 
//				new Date(), new Date());
//		Trade testTrade = new Trade(10, 20, TradeType.BUY, tmaStrategy);
//		restTemplate.postForEntity(tradeBasePath, 
//				testTrade, Trade.class);
//
//		ResponseEntity<List<Trade>> findAllResponse = restTemplate.exchange(
//                    tradeBasePath,
//                    HttpMethod.GET,
//                    null,
//                    new ParameterizedTypeReference<List<Trade>>(){});
//
//		assertEquals(findAllResponse.getStatusCode(), HttpStatus.OK);
//		assertEquals(findAllResponse.getBody().get(0).getSize(), testTrade.getSize());
//	}
//
//	@Test
//	@Transactional
//	public void findById_returnsCorrectId() {
//		tmaStrategy = new TMAStrategy(stock, 20, new Date(), new Date(), 
//				new Date(), new Date());
//		Trade testTrade = new Trade(10, 20, TradeType.BUY, tmaStrategy);
//		ResponseEntity<Trade> createdResponse =
//				restTemplate.postForEntity(tradeBasePath,
//                               testTrade, Trade.class);
//
//		assertEquals(createdResponse.getStatusCode(), HttpStatus.CREATED);
//
//		Trade foundTrade = restTemplate.getForObject(
//                    tradeBasePath + "/" + createdResponse.getBody().getId(),
//                    Trade.class);
//
//		assertEquals(foundTrade.getId(), createdResponse.getBody().getId());
//		assertEquals(foundTrade.getSize(), testTrade.getSize());
//	}
//
//	@Test
//	@Transactional
//	public void deleteById_deletes() {
//		tmaStrategy = new TMAStrategy(stock, 20, new Date(), new Date(), 
//				new Date(), new Date());
//		Trade testTrade = new Trade(10, 20, TradeType.BUY, tmaStrategy);
//		ResponseEntity<Trade> createdResponse =
//				restTemplate.postForEntity(tradeBasePath,
//                               testTrade, Trade.class);
//
//		assertEquals(createdResponse.getStatusCode(), HttpStatus.CREATED);
//
//		Trade foundTrade = restTemplate.getForObject(
//                    tradeBasePath + "/" + createdResponse.getBody().getId(),
//                    Trade.class);
//
//		logger.debug("Before delete, findById gives: " + foundTrade);
//		assertNotNull(foundTrade);
//
//		restTemplate.delete(tradeBasePath + "/" + createdResponse.getBody().getId());
//
//		ResponseEntity<Trade> response = restTemplate.exchange(
//                    tradeBasePath + "/" + createdResponse.getBody().getId(),
//                    HttpMethod.GET,
//                    null,
//                    Trade.class);
//
//		logger.debug("After delete, findById response code is: " +
//				response.getStatusCode());
//		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
//	}
}
