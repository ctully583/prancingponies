package com.citi.training.trader.service;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trader.dao.mysql.MysqlPriceDao;
import com.citi.training.trader.dao.mysql.MysqlStockDao;
import com.citi.training.trader.model.Price;
import com.citi.training.trader.model.Stock;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("h2")
public class PriceReaderTests {

	@Autowired
	private PriceReader priceReader;
	
	@Autowired
	private MysqlPriceDao mysqlPriceDao;
	
	@Autowired
	private MysqlStockDao mysqlStockDao;
	
	private Stock stock;
	
	@Before
	public void setUp() throws Exception {
		stock = new Stock(1, "APPL");
	}

	@Test
	@Transactional
	public void testReadPrices() {
		int stockId = mysqlStockDao.create(stock);
		priceReader.readPrices();
		List<Price> prices = mysqlPriceDao.findAll(mysqlStockDao.findById(stockId));
		assert(prices.size() > 0);
	}

}
