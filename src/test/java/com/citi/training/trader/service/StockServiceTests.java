package com.citi.training.trader.service;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trader.exceptions.StockNotFoundException;
import com.citi.training.trader.model.Stock;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("h2")
public class StockServiceTests {

	private Stock stock;
	
	@Autowired
	private StockService stockService;
	
	@Before
	public void setUp() throws Exception {
		stock = new Stock(1, "APPL");
	}

	@Test
	@Transactional
	public void testCreateAndFindAll() {
		stockService.create(stock);
		List<Stock> stocks = stockService.findAll();
		assert(stocks.size() > 0);
	}

	@Test 
	@Transactional
	public void testFindById() {
		int stockId = stockService.create(stock);
		Stock returnedStock = stockService.findById(stockId);
		assert(returnedStock.getId() == stockId);
		assert(returnedStock.getTicker() == "APPL");
	}

	@Test
	@Transactional
	public void testFindByTicker() {
		int stockId = stockService.create(stock);
		Stock returnedStock = stockService.findByTicker(stock.getTicker());
		assert(returnedStock.getId() == stockId);
		assert(returnedStock.getTicker() == "APPL");
	}

	@Test (expected = StockNotFoundException.class)
	@Transactional
	public void testDeleteById() {
		int stockId = stockService.create(stock);
		stockService.deleteById(stockId);
		stockService.findById(stockId);
	}

}
