package com.citi.training.trader.service;

import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trader.dao.mysql.MysqlStockDao;
import com.citi.training.trader.dao.mysql.MysqlTMAStrategyDao;
import com.citi.training.trader.exceptions.TradeNotFoundException;
import com.citi.training.trader.model.Stock;
import com.citi.training.trader.model.TMAStrategy;
import com.citi.training.trader.model.Trade;
import com.citi.training.trader.model.Trade.TradeState;
import com.citi.training.trader.model.Trade.TradeType;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("h2")
public class TradeServiceTests {

	@Autowired
	private TradeService tradeService;
	
	@Autowired
	private MysqlStockDao mysqlStockDao;
	
	@Autowired
	private MysqlTMAStrategyDao mysqlTMAStrategyDao;
	
	private Trade trade;
	private TMAStrategy tmaStrategy;
	private Stock stock;
	
	private double price = 10.0;
	private int size = 20;
	private TradeType tradeType = TradeType.BUY;
	
	@Before
	public void setUp() throws Exception {
		stock = new Stock(1, "APPL");
	}

	@Test
	@Transactional
	public void testSaveAndFindAll() {
		int stockId = mysqlStockDao.create(stock);
		tmaStrategy = new TMAStrategy(mysqlStockDao.findById(stockId), size, new Date(), 
				new Date(), new Date(), new Date());
		int strategyId = mysqlTMAStrategyDao.save(tmaStrategy);
		trade = new Trade(price, size, tradeType, mysqlTMAStrategyDao.findById(strategyId));
		tradeService.save(trade);
		List<Trade> trades = tradeService.findAll();
		assert(trades.size() > 0);
	}

	@Test
	@Transactional
	public void testFindById() {
		int stockId = mysqlStockDao.create(stock);
		tmaStrategy = new TMAStrategy(mysqlStockDao.findById(stockId), size, new Date(), 
				new Date(), new Date(), new Date());
		int strategyId = mysqlTMAStrategyDao.save(tmaStrategy);
		trade = new Trade(price, size, tradeType, mysqlTMAStrategyDao.findById(strategyId));
		int tradeId = tradeService.save(trade);
		Trade returnedTrade = tradeService.findById(tradeId);
		assert(returnedTrade.getId() == tradeId);
		assert(returnedTrade.getPrice() == price);
	}

	@Test
	@Transactional
	public void testFindAllByState() {
		int stockId = mysqlStockDao.create(stock);
		tmaStrategy = new TMAStrategy(mysqlStockDao.findById(stockId), size, new Date(), 
				new Date(), new Date(), new Date());
		int strategyId = mysqlTMAStrategyDao.save(tmaStrategy);
		trade = new Trade(price, size, tradeType, mysqlTMAStrategyDao.findById(strategyId));
		tradeService.save(trade);
		List<Trade> trades = tradeService.findAllByState(TradeState.INIT);
		assert(trades.size() > 0);
	}

	@Test
	@Transactional
	public void testFindLatestByStrategyId() {
		int stockId = mysqlStockDao.create(stock);
		tmaStrategy = new TMAStrategy(mysqlStockDao.findById(stockId), size, new Date(), 
				new Date(), new Date(), new Date());
		int strategyId = mysqlTMAStrategyDao.save(tmaStrategy);
		trade = new Trade(price, size, tradeType, mysqlTMAStrategyDao.findById(strategyId));
		tradeService.save(trade);
		Trade returnedTrade = tradeService.findLatestByStrategyId(strategyId);
		assert(returnedTrade.getStrategy().getId() == strategyId);
		assert(returnedTrade.getPrice() == price);
	}

	@Test (expected = TradeNotFoundException.class)
	@Transactional
	public void testDeleteById() {
		int stockId = mysqlStockDao.create(stock);
		tmaStrategy = new TMAStrategy(mysqlStockDao.findById(stockId), size, new Date(), 
				new Date(), new Date(), new Date());
		int strategyId = mysqlTMAStrategyDao.save(tmaStrategy);
		trade = new Trade(price, size, tradeType, mysqlTMAStrategyDao.findById(strategyId));
		int tradeId = tradeService.save(trade);
		tradeService.deleteById(tradeId);
		tradeService.findById(tradeId);
		
	}

}
