package com.citi.training.trader.service;

import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trader.dao.mysql.MysqlStockDao;
import com.citi.training.trader.model.Price;
import com.citi.training.trader.model.Stock;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("h2")
public class PriceServiceTests {

	@Autowired
	private PriceService priceService;
	
	@Autowired
	private MysqlStockDao mysqlStockDao;
	
	private Stock stock;
	private Price price;
	
	@Before
	public void setUp() throws Exception {
		stock = new Stock(1, "APPL");
		
	}

	@Test
	@Transactional
	public void testCreateAndFindLatest() {
		int stockId = mysqlStockDao.create(stock);
		price = new Price(mysqlStockDao.findById(stockId), 10);
		int id = priceService.create(price);
		List<Price> prices = priceService.findLatest(stock, 1);
		assert(prices.get(0).getId() == id);
	}

	@Test
	@Transactional
	public void testFindAll() {
		int stockId = mysqlStockDao.create(stock);
		price = new Price(mysqlStockDao.findById(stockId), 10);
		priceService.create(price);
		List<Price> prices = priceService.findAll(mysqlStockDao.findById(stockId));
		assert(prices.size() > 0);
	}

	@Test
	@Transactional
	public void testDeleteOlderThan() {
		int stockId = mysqlStockDao.create(stock);
		price = new Price(mysqlStockDao.findById(stockId), 10);
		priceService.create(price);
		priceService.deleteOlderThan(new Date());
		List<Price> prices = priceService.findAll(mysqlStockDao.findById(stockId));
		assert(prices.size() == 0);
	}

}
