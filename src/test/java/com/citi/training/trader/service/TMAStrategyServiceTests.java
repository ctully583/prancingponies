package com.citi.training.trader.service;

import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trader.dao.mysql.MysqlStockDao;
import com.citi.training.trader.model.Stock;
import com.citi.training.trader.model.TMAStrategy;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("h2")
public class TMAStrategyServiceTests {
	
	@Autowired
	private TMAStrategyService tmaStrategyService;
	
	@Autowired
	private MysqlStockDao mysqlStockDao;
	
	private TMAStrategy tmaStrategy;
	private Stock stock;
	private int size = 10;
	
	@Before
	public void setUp() throws Exception {
		stock = new Stock(1, "APPL");
	}

	@Test
	@Transactional
	public void testSaveAndFindAll() {
		int stockId = mysqlStockDao.create(stock);
		tmaStrategy = new TMAStrategy(mysqlStockDao.findById(stockId), size, 
				new Date(), new Date(), new Date(), new Date());
		tmaStrategyService.save(tmaStrategy);
		List<TMAStrategy> strategies = tmaStrategyService.findAll();
		assert(strategies.size() > 0);
	}
}
