package com.citi.training.trader.dao.mysql;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trader.exceptions.StockNotFoundException;
import com.citi.training.trader.model.Stock;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("h2")
public class MysqlStockDaoTests {

	@Autowired
	MysqlStockDao mysqlStockDao;
	
	private Stock stock;
	
	@Before
	public void setUp() throws Exception {
		stock = new Stock(-1, "APPL");
	}

	@Test
	@Transactional
	public void testCreateAndFindAll() {
		mysqlStockDao.create(stock);
		List<Stock> stocks = mysqlStockDao.findAll();
		assert(stocks.size() >= 1);
	}

	@Test
	@Transactional
	public void testFindByIdSuccessful() {
		int id = mysqlStockDao.create(stock);
		Stock stock = mysqlStockDao.findById(id);
		assert(stock.getId() == id);
		assert(stock.getTicker() == "APPL");
	}
	
	@Test (expected = StockNotFoundException.class)
	@Transactional
	public void testFindByIdUnsuccessful() {
		int id = 10;
		mysqlStockDao.findById(id);
	}

	@Test
	@Transactional
	public void testFindByTickerSuccessful() {
		int id = mysqlStockDao.create(stock);
		Stock returnedStock = mysqlStockDao.findByTicker(stock.getTicker());
		assert(returnedStock.getId() == id);
	}
	
	@Test (expected = StockNotFoundException.class)
	@Transactional
	public void testFindByTickerUnsuccessful() {
		mysqlStockDao.findByTicker("AMAZ");
	}

	@Test (expected = StockNotFoundException.class)
	@Transactional
	public void testDeleteByIdSuccessful() {
		int id = mysqlStockDao.create(stock);
		mysqlStockDao.deleteById(id);
		mysqlStockDao.findById(id);
	}
	
	@Test (expected = StockNotFoundException.class)
	@Transactional
	public void testDeleteByIdUnsuccessful() {
		mysqlStockDao.deleteById(10);
	}
}
