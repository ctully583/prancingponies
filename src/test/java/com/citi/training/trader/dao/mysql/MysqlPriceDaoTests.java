package com.citi.training.trader.dao.mysql;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trader.model.Price;
import com.citi.training.trader.model.Stock;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("h2")
public class MysqlPriceDaoTests {

	@Autowired
	MysqlPriceDao mysqlPriceDao;
	
	@Autowired
	MysqlStockDao mysqlStockDao;
	
	private Stock testStock;
	private Price testPrice;
	
	@Before
	public void setUp() throws Exception {
		testStock = new Stock(-1, "FVJ");
		testPrice = new Price(testStock, 10.0);
	}

	@Test
	@Transactional
	public void testCreateAndFindAll() {
		int id = mysqlStockDao.create(testStock);
		testPrice = new Price(mysqlStockDao.findById(id), 10.0);
		mysqlPriceDao.create(testPrice);
		Stock stock = mysqlStockDao.findById(id);
		assertTrue(mysqlPriceDao.findAll(stock).size() >= 1);
	}

//	@Test
//	@Transactional
//	public void testFindLatest() {
//		int id = mysqlStockDao.create(testStock);
//		testPrice = new Price(mysqlStockDao.findById(id), 10.0);
//		mysqlPriceDao.create(testPrice);
//		List<Price> prices = new ArrayList<Price>();
//		prices = mysqlPriceDao.findLatest(mysqlStockDao.findById(id), 1);
//		assert(prices.get(0).getId() == id);
//		assert(prices.get(0).getPrice() == 10.0);
//	}

	@Test
	@Transactional
	public void testDeleteOlderThan() {
		int id = mysqlStockDao.create(testStock);
		testPrice = new Price(mysqlStockDao.findById(id), 10.0);
		mysqlPriceDao.create(testPrice);
		int numDeleted = mysqlPriceDao.deleteOlderThan(new Date());
		assert(numDeleted > 0);
	}

}
