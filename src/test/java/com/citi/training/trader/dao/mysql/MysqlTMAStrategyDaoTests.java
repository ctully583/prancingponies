package com.citi.training.trader.dao.mysql;

import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trader.exceptions.TMAStrategyNotFoundException;
import com.citi.training.trader.model.Stock;
import com.citi.training.trader.model.TMAStrategy;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("h2")
public class MysqlTMAStrategyDaoTests {

	@Autowired
	MysqlStockDao mysqlStockDao;
	
	@Autowired
	MysqlTMAStrategyDao mysqlTMAStrategyDao;
	
	private Stock stock;
	
	@Before
	public void setUp() throws Exception {
		stock = new Stock(-1, "APPL");
	}
	
	@Test
	@Transactional
	public void testSaveAndFindAll() {
		int stockId = mysqlStockDao.create(stock);
		mysqlTMAStrategyDao.save(
				new TMAStrategy(mysqlStockDao.findById(stockId), 100, new Date(), 
						new Date(), new Date(), new Date()));
		List<TMAStrategy> strategies = mysqlTMAStrategyDao.findAll();
		assert(strategies.size() > 0);
	}
	
	@Test
	@Transactional
	public void testSaveUpdateAndFindAll() {
		int stockId = mysqlStockDao.create(stock);
		int strategyId = mysqlTMAStrategyDao.save(
				new TMAStrategy(mysqlStockDao.findById(stockId), 100, new Date(), 
						new Date(), new Date(), new Date()));
		mysqlTMAStrategyDao.save(mysqlTMAStrategyDao.findById(strategyId));
		List<TMAStrategy> strategies = mysqlTMAStrategyDao.findAll();
		assert(strategies.size() > 0);
	}

	@Test
	@Transactional
	public void testFindByIdSuccessful() {
		int stockId = mysqlStockDao.create(stock);
		int strategyId = mysqlTMAStrategyDao.save(
				new TMAStrategy(mysqlStockDao.findById(stockId), 100, new Date(), 
						new Date(), new Date(), new Date()));
		TMAStrategy strategy = mysqlTMAStrategyDao.findById(strategyId);
		assert(strategyId == strategy.getId());
		assert(stock.getTicker().equals(strategy.getStock().getTicker()));
		assert(100 == strategy.getSize());
	}
	
	@Test (expected = TMAStrategyNotFoundException.class)
	@Transactional
	public void testFindByIdUnsuccessful() {
		mysqlTMAStrategyDao.findById(10);
	}
}
