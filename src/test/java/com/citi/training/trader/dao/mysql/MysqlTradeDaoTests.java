package com.citi.training.trader.dao.mysql;

import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trader.exceptions.TradeNotFoundException;
import com.citi.training.trader.model.Stock;
import com.citi.training.trader.model.TMAStrategy;
import com.citi.training.trader.model.Trade;
import com.citi.training.trader.model.Trade.TradeState;
import com.citi.training.trader.model.Trade.TradeType;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("h2")
public class MysqlTradeDaoTests {

	@Autowired
	MysqlTradeDao mysqlTradeDao;
	
	@Autowired
	MysqlStockDao mysqlStockDao;
	
	@Autowired
	MysqlTMAStrategyDao mysqlTmaStrategyDao;
	
	private Stock stock;
	
	@Before
	public void setUp() throws Exception {
		stock = new Stock(-1, "APPL");
	}

	@Test
	@Transactional
	public void testSaveAndFindAll() {
		int stockId = mysqlStockDao.create(stock);
		int tmaStrategyId = 
				mysqlTmaStrategyDao.save(
						new TMAStrategy(mysqlStockDao.findById(stockId), 100, 
								new Date(), new Date(), new Date(), new Date()));
		mysqlTradeDao.save(new Trade(20.0, 100, TradeType.BUY, 
				mysqlTmaStrategyDao.findById(tmaStrategyId)));
		List<Trade> trades = mysqlTradeDao.findAll();
		assert(trades.size() > 0);
	}
	
	@Test
	@Transactional
	public void testSaveUpdateAndFindAll() {
		int stockId = mysqlStockDao.create(stock);
		int tmaStrategyId = 
				mysqlTmaStrategyDao.save(
						new TMAStrategy(mysqlStockDao.findById(stockId), 100, 
								new Date(), new Date(), new Date(), new Date()));
		int tradeId = mysqlTradeDao.save(new Trade(20.0, 100, TradeType.BUY, 
				mysqlTmaStrategyDao.findById(tmaStrategyId)));
		mysqlTradeDao.save(mysqlTradeDao.findById(tradeId));
		List<Trade> trades = mysqlTradeDao.findAll();
		assert(trades.size() > 0);
	}

	@Test
	@Transactional
	public void testFindById() {
		int stockId = mysqlStockDao.create(stock);
		int simpleStrategyId = 
				mysqlTmaStrategyDao.save(new TMAStrategy(
						mysqlStockDao.findById(stockId), 100, new Date(), new Date(), 
						new Date(), new Date()));
		int tradeId = mysqlTradeDao.save(new Trade(20.0, 100, TradeType.BUY, 
				mysqlTmaStrategyDao.findById(simpleStrategyId)));
		Trade trade = mysqlTradeDao.findById(tradeId);
		assert(trade.getId() == tradeId);
		assert(trade.getPrice() == 20.0);
		assert(trade.getSize() == 100);
	}

	@Test
	@Transactional
	public void testFindAllByState() {
		int stockId = mysqlStockDao.create(stock);
		int simpleStrategyId = 
				mysqlTmaStrategyDao.save(
						new TMAStrategy(mysqlStockDao.findById(stockId), 100, 
								new Date(), new Date(), new Date(), new Date()));
		mysqlTradeDao.save(new Trade(20.0, 100, TradeType.BUY, 
				mysqlTmaStrategyDao.findById(simpleStrategyId)));
		List<Trade> trades = mysqlTradeDao.findAllByState(TradeState.INIT);
		assert(trades.size() > 0);
	}

	@Test
	@Transactional
	public void testFindLatestByStrategyId() {
		int stockId = mysqlStockDao.create(stock);
		int tmaStrategyId = 
				mysqlTmaStrategyDao.save(
						new TMAStrategy(mysqlStockDao.findById(stockId), 100, 
								new Date(), new Date(), new Date(), new Date()));
		int tradeId = mysqlTradeDao.save(new Trade(20.0, 100, TradeType.BUY, 
				mysqlTmaStrategyDao.findById(tmaStrategyId)));
		Trade trade = mysqlTradeDao.findLatestByStrategyId(tmaStrategyId);
		assert(trade.getId() == tradeId);
		assert(trade.getPrice() == 20.0);
		assert(trade.getSize() == 100);
	}
	
	@Test (expected = TradeNotFoundException.class)
	@Transactional
	public void testFindLatestByStrategyIdUnsuccessful() {
		mysqlTradeDao.findLatestByStrategyId(10);
	}

	@Test (expected = TradeNotFoundException.class)
	@Transactional
	public void testDeleteById() {
		int stockId = mysqlStockDao.create(stock);
		int tmaStrategyId = 
				mysqlTmaStrategyDao.save(
						new TMAStrategy(mysqlStockDao.findById(stockId), 100, 
								new Date(), new Date(), new Date(), new Date()));
		int tradeId = mysqlTradeDao.save(new Trade(20.0, 100, TradeType.BUY, 
				mysqlTmaStrategyDao.findById(tmaStrategyId)));
		mysqlTradeDao.deleteById(tradeId);
		mysqlTradeDao.findById(tradeId);
	}

	@Test (expected = TradeNotFoundException.class)
	@Transactional
	public void testDeleteByIdUnsuccessful() {
		int tradeId = 1;
		mysqlTradeDao.deleteById(tradeId);
	}
}
