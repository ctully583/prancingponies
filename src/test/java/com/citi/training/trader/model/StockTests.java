package com.citi.training.trader.model;

import org.junit.Test;

public class StockTests {

	private Stock stock;
	private int id = 1;
	private String ticker = "APPL";

	@Test
	public void testStock() {
		stock = new Stock(id, ticker);
		assert(id == stock.getId());
		assert(ticker.equals(stock.getTicker()));
	}

	@Test
	public void testGetId() {
		stock = new Stock(id, ticker);
		assert(id == stock.getId());
	}

	@Test
	public void testSetId() {
		int newId = 2;
		stock = new Stock(id, ticker);
		stock.setId(newId);
		assert(newId == stock.getId());
	}

	@Test
	public void testGetTicker() {
		stock = new Stock(id, ticker);
		assert(ticker.equals(stock.getTicker()));
	}

	@Test
	public void testSetTicker() {
		String newTicker = "GOOG";
		stock = new Stock(id, ticker);
		stock.setTicker(newTicker);
		assert(newTicker.equals(stock.getTicker()));
	}

	@Test
	public void testToString() {
		String testString = new Stock(id, ticker).toString();
		assert(testString.contains(new Integer(id).toString()));
		assert(testString.contains(new String(ticker).toString()));
	}

}
