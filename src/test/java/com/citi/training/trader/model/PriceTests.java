package com.citi.training.trader.model;

import java.util.Date;

import org.junit.Test;

public class PriceTests {

	private Price priceObj;
	private int id = 1;
	private Stock stock = new Stock(1, "APPL");
	private double price = 10.0;
	private Date recordedAt = new Date();

	@Test
	public void testPriceStockDouble() {
		priceObj = new Price(stock, price);
		assert(stock.getId() == priceObj.getStock().getId());
		assert(stock.getTicker()).equals(priceObj.getStock().getTicker());
		assert(price == priceObj.getPrice());
	}

	@Test
	public void testPriceStockDoubleDate() {
		priceObj = new Price(stock, price, recordedAt);
		assert(stock.getId() == priceObj.getStock().getId());
		assert(stock.getTicker().equals(priceObj.getStock().getTicker()));
		assert(price == priceObj.getPrice());
		assert(recordedAt.equals(priceObj.getRecordedAt()));
	}

	@Test
	public void testPriceIntStockDoubleDate() {
		priceObj = new Price(id, stock, price, recordedAt);
		assert(id == priceObj.getId());
		assert(stock.getId() == priceObj.getStock().getId());
		assert(stock.getTicker().equals(priceObj.getStock().getTicker()));
		assert(price == priceObj.getPrice());
		assert(recordedAt.equals(priceObj.getRecordedAt()));
	}

	@Test
	public void testGetId() {
		priceObj = new Price(id, stock, price, recordedAt);
		assert(id == priceObj.getId());
	}

	@Test
	public void testSetId() {
		int newId = 2;
		priceObj = new Price(id, stock, price, recordedAt);
		priceObj.setId(newId);
		assert(newId == priceObj.getId());
	}

	@Test
	public void testGetStock() {
		priceObj = new Price(id, stock, price, recordedAt);
		assert(stock.equals(priceObj.getStock()));
	}

	@Test
	public void testSetStock() {
		Stock newStock = new Stock(2, "GOOG");
		priceObj = new Price(id, stock, price, recordedAt);
		priceObj.setStock(newStock);
		assert(newStock.equals(priceObj.getStock()));
	}

	@Test
	public void testGetPrice() {
		priceObj = new Price(id, stock, price, recordedAt);
		assert(price == priceObj.getPrice());
	}

	@Test
	public void testSetPrice() {
		double newPrice = 20.0;
		priceObj = new Price(id, stock, price, recordedAt);
		priceObj.setPrice(newPrice);
		assert(newPrice == priceObj.getPrice());
	}

	@Test
	public void testGetRecordedAt() {
		priceObj = new Price(id, stock, price, recordedAt);
		assert(recordedAt.equals(priceObj.getRecordedAt()));
	}

	@Test
	public void testSetRecordedAt() {
		Date newDate = new Date();
		priceObj = new Price(id, stock, price, recordedAt);
		priceObj.setRecordedAt(newDate);
		assert(newDate.equals(priceObj.getRecordedAt()));
	}

	@Test
	public void testToString() {
		String testString = new Price(id, stock, price, recordedAt).toString();
		assert(testString.contains(new Integer(id).toString()));
		assert(testString.contains(new Stock(id, stock.getTicker()).toString()));
		assert(testString.contains(new Double(price).toString()));
	}

}
