package com.citi.training.trader.model;

import java.time.LocalDateTime;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import com.citi.training.trader.model.Trade.TradeState;
import com.citi.training.trader.model.Trade.TradeType;

public class TradeTests {

	Trade trade;
	TMAStrategy tmaStrategy;

	private double price = 1.1;
	private int size = 1;
	private LocalDateTime lastStateChanged;
	private boolean accountedFor = true;

	private int id = 1;
	private double longAverage = 1.1;
	private double shortAverage = 1.2;
	private Stock stock;
	private Date longPeriodStart;
	private Date longPeriodEnd;
	private Date shortPeriodStart;
	private Date shortPeriodEnd;
	private Double profitLossPer = 10.0;
	private Date stopped;

	@Before
	public void setUp() throws Exception {
		stock = new Stock(1, "test");
		tmaStrategy = new TMAStrategy(1, stock, size, longAverage, shortAverage, longPeriodStart, longPeriodEnd,
				shortPeriodStart, shortPeriodEnd, profitLossPer, stopped);
	}

	/**
	 * Testing Constructor 1
	 */
	@Test
	public void testTrade() {

		// when testing empty constructor use size
		Trade trade = new Trade();
		assert (trade.getSize() == 0);
	}

	/**
	 * Testing Constructor 2
	 */
	@Test
	public void testTradeDoubleIntTradeTypeTMAStrategy() {
		trade = new Trade(price, size, TradeType.BUY, tmaStrategy);

		assert (trade.getPrice() == 1.1);
		assert (trade.getSize() == 1);
		assert (trade.getTradeType() == TradeType.BUY);

		assert (tmaStrategy.getSize() == trade.getStrategy().getSize());
		assert (tmaStrategy.getId() == trade.getStrategy().getId());
		assert (tmaStrategy.getStock() == trade.getStrategy().getStock());
		assert (tmaStrategy.getLongAverage() == trade.getStrategy().getLongAverage());
		assert (tmaStrategy.getShortAverage() == trade.getStrategy().getShortAverage());
		assert (tmaStrategy.getLongPeriodStart() == trade.getStrategy().getLongPeriodStart());
		assert (tmaStrategy.getLongPeriodEnd() == trade.getStrategy().getShortPeriodEnd());
		assert (tmaStrategy.getShortPeriodEnd() == trade.getStrategy().getShortPeriodEnd());
		assert (tmaStrategy.getShortPeriodStart() == trade.getStrategy().getShortPeriodStart());
		assert (tmaStrategy.getProfitLossPer() == trade.getStrategy().getProfitLossPer());
		assert (tmaStrategy.getStopped() == trade.getStrategy().getStopped());

	}

	/**
	 * Testing Constructor 3
	 */
	@Test
	public void testTradeIntDoubleIntStringStringLocalDateTimeBooleanTMAStrategy() {
		trade = new Trade(id, price, size, TradeType.BUY, TradeState.INIT, lastStateChanged, accountedFor, tmaStrategy);

		assert (trade.getId() == 1);
		assert (trade.getPrice() == 1.1);
		assert (trade.getSize() == 1);
		assert (trade.getTradeType() == TradeType.BUY);
		assert (trade.getState() == TradeState.INIT);
		assert (trade.getLastStateChange() == lastStateChanged);
		assert (trade.getAccountedFor() == true);

		assert (tmaStrategy.getSize() == trade.getStrategy().getSize());
		assert (tmaStrategy.getId() == trade.getStrategy().getId());
		assert (tmaStrategy.getStock() == trade.getStrategy().getStock());
		assert (tmaStrategy.getLongAverage() == trade.getStrategy().getLongAverage());
		assert (tmaStrategy.getShortAverage() == trade.getStrategy().getShortAverage());
		assert (tmaStrategy.getLongPeriodStart() == trade.getStrategy().getLongPeriodStart());
		assert (tmaStrategy.getLongPeriodEnd() == trade.getStrategy().getShortPeriodEnd());
		assert (tmaStrategy.getShortPeriodEnd() == trade.getStrategy().getShortPeriodEnd());
		assert (tmaStrategy.getShortPeriodStart() == trade.getStrategy().getShortPeriodStart());
		assert (tmaStrategy.getProfitLossPer() == trade.getStrategy().getProfitLossPer());
		assert (tmaStrategy.getStopped() == trade.getStrategy().getStopped());

	}

	/**
	 * Testing Constructor 4
	 */
	@Test
	public void testTradeIntDoubleIntTradeTypeTradeStateLocalDateTimeBooleanTMAStrategy() {
		trade = new Trade(id, price, size, TradeType.BUY, TradeState.INIT, lastStateChanged, accountedFor, tmaStrategy);

		assert (trade.getId() == 1);
		assert (trade.getPrice() == 1.1);
		assert (trade.getSize() == 1);
		assert (trade.getTradeType() == TradeType.BUY);
		assert (trade.getState() == TradeState.INIT);
		assert (trade.getLastStateChange() == lastStateChanged);
		assert (trade.getAccountedFor() == true);

		assert (tmaStrategy.getSize() == trade.getStrategy().getSize());
		assert (tmaStrategy.getId() == trade.getStrategy().getId());
		assert (tmaStrategy.getStock() == trade.getStrategy().getStock());
		assert (tmaStrategy.getLongAverage() == trade.getStrategy().getLongAverage());
		assert (tmaStrategy.getShortAverage() == trade.getStrategy().getShortAverage());
		assert (tmaStrategy.getLongPeriodStart() == trade.getStrategy().getLongPeriodStart());
		assert (tmaStrategy.getLongPeriodEnd() == trade.getStrategy().getShortPeriodEnd());
		assert (tmaStrategy.getShortPeriodEnd() == trade.getStrategy().getShortPeriodEnd());
		assert (tmaStrategy.getShortPeriodStart() == trade.getStrategy().getShortPeriodStart());
		assert (tmaStrategy.getProfitLossPer() == trade.getStrategy().getProfitLossPer());
		assert (tmaStrategy.getStopped() == trade.getStrategy().getStopped());

	}

	@Test
	public void testGetId() {
		trade = new Trade(id, price, size, TradeType.BUY, TradeState.INIT, lastStateChanged, accountedFor, tmaStrategy);
		assert (trade.getId() == id);
	}

	@Test
	public void testSetId() {
		trade = new Trade(id, price, size, TradeType.BUY, TradeState.INIT, lastStateChanged, accountedFor, tmaStrategy);
		trade.setId(10);
		assert (trade.getId() == 10);
	}

	@Test
	public void testGetStockTicker() {
		trade = new Trade(id, price, size, TradeType.BUY, TradeState.INIT, lastStateChanged, accountedFor, tmaStrategy);
		assert (trade.getStrategy().getStock().getTicker().equals("test"));
	}

	@Test
	public void testGetTradeType() {
		trade = new Trade(id, price, size, TradeType.BUY, TradeState.INIT, lastStateChanged, accountedFor, tmaStrategy);
		assert(trade.getTradeType() == TradeType.BUY);
	}

	@Test
	public void testSetTradeType() {
		trade = new Trade(id, price, size, TradeType.BUY, TradeState.INIT, lastStateChanged, accountedFor, tmaStrategy);
		trade.setTradeType(TradeType.SELL);
		assert(trade.getTradeType() == TradeType.SELL);
	}

	@Test
	public void testGetTradeTypeXml() {
		trade = new Trade(id, price, size, TradeType.BUY, TradeState.INIT, lastStateChanged, accountedFor, tmaStrategy);
		String returnedString = trade.getTradeTypeXml();
		assert(returnedString != null);
	}

	@Test
	public void testSetTradeTypeXml() {
		trade = new Trade(id, price, size, TradeType.BUY, TradeState.INIT, lastStateChanged, accountedFor, tmaStrategy);
		String returnedString = trade.getTradeTypeXml();
		trade.setTradeTypeXml(returnedString);
		assert(trade.getTradeTypeXml() != null);
	}

	@Test
	public void testGetPrice() {
		trade = new Trade(id, price, size, TradeType.BUY, TradeState.INIT, lastStateChanged, accountedFor, tmaStrategy);
		assert(trade.getPrice() == 1.1);
	}

	@Test
	public void testSetPrice() {
		trade = new Trade(id, price, size, TradeType.BUY, TradeState.INIT, lastStateChanged, accountedFor, tmaStrategy);
		trade.setPrice(3.333);
		assert(trade.getPrice() == 3.333);
	}

	@Test
	public void testGetSize() {
		trade = new Trade(id, price, size, TradeType.BUY, TradeState.INIT, lastStateChanged, accountedFor, tmaStrategy);
		assert(trade.getSize() == 1);
	}

	@Test
	public void testSetSize() {
		trade = new Trade(id, price, size, TradeType.BUY, TradeState.INIT, lastStateChanged, accountedFor, tmaStrategy);
		trade.setSize(2);
		assert(trade.getSize() == 2);
	}

	@Test
	public void testGetStrategy() {
		trade = new Trade(id, price, size, TradeType.BUY, TradeState.INIT, lastStateChanged, accountedFor, tmaStrategy);
		assert(trade.getStrategy() == tmaStrategy);
	}

	@Test
	public void testSetStrategy() {
		trade = new Trade(id, price, size, TradeType.BUY, TradeState.INIT, lastStateChanged, accountedFor, tmaStrategy);
		trade.setStrategy(tmaStrategy);
		assert(trade.getStrategy() == tmaStrategy);
	}

	@Test
	public void testGetLastStateChange() {
		trade = new Trade(id, price, size, TradeType.BUY, TradeState.INIT, lastStateChanged, accountedFor, tmaStrategy);
		assert(trade.getLastStateChange() == lastStateChanged);
	}

	@Test
	public void testSetLastStateChange() {
		trade = new Trade(id, price, size, TradeType.BUY, TradeState.INIT, lastStateChanged, accountedFor, tmaStrategy);
		trade.setLastStateChange(lastStateChanged);
		assert(trade.getLastStateChange() == lastStateChanged);
	}

	@Test
	public void testGetLastStateChangeXml() {
		trade = new Trade(id, price, size, TradeType.BUY, TradeState.INIT, lastStateChanged, accountedFor, tmaStrategy);
		
	}

	@Test
	public void testSetLastStateChangeXml() {
		trade = new Trade(id, price, size, TradeType.BUY, TradeState.INIT, lastStateChanged, accountedFor, tmaStrategy);

	}

	@Test
	public void testGetState() {
		trade = new Trade(id, price, size, TradeType.BUY, TradeState.INIT, lastStateChanged, accountedFor, tmaStrategy);
		assert(trade.getState() == TradeState.INIT);
	}

	@Test
	public void testSetState() {
		trade = new Trade(id, price, size, TradeType.BUY, TradeState.INIT, lastStateChanged, accountedFor, tmaStrategy);
		
	}

	@Test
	public void testStateChange() {
		trade = new Trade(id, price, size, TradeType.BUY, TradeState.INIT, lastStateChanged, accountedFor, tmaStrategy);

	}

	@Test
	public void testGetStateXml() {
		trade = new Trade(id, price, size, TradeType.BUY, TradeState.INIT, lastStateChanged, accountedFor, tmaStrategy);

	}

	@Test
	public void testSetStateXml() {
		trade = new Trade(id, price, size, TradeType.BUY, TradeState.INIT, lastStateChanged, accountedFor, tmaStrategy);
;
	}

	@Test
	public void testGetAccountedFor() {
		trade = new Trade(id, price, size, TradeType.BUY, TradeState.INIT, lastStateChanged, accountedFor, tmaStrategy);

	}

	@Test
	public void testSetAccountedFor() {
		trade = new Trade(id, price, size, TradeType.BUY, TradeState.INIT, lastStateChanged, accountedFor, tmaStrategy);
		
	}

	@Test
	public void testToString() {
		String testString = 
				new Trade(id, price, size, TradeType.BUY, TradeState.INIT, lastStateChanged, 
						accountedFor, tmaStrategy).toString();
		assert(testString.contains(new Integer(size).toString()));
	}

	@Test
	public void testEqualsObject() {
		trade = new Trade(id, price, size, TradeType.BUY, TradeState.INIT, lastStateChanged, accountedFor, tmaStrategy);

	}

	@Test
	public void testToXml() {
		trade = new Trade(id, price, size, TradeType.BUY, TradeState.INIT, lastStateChanged, accountedFor, tmaStrategy);

	}

	@Test
	public void testFromXml() {
		trade = new Trade(id, price, size, TradeType.BUY, TradeState.INIT, lastStateChanged, accountedFor, tmaStrategy);

	}

}
