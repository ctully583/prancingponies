package com.citi.training.trader.model;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;

public class TMAStrategyTests {

	private TMAStrategy tmaStrategy;
	
	private int id = 1;
    private Stock stock;
    private int size = 2;
    private double longAverage = 1.1;
    private double shortAverage = 1.2;
    private Date longPeriodStart = new Date();
    private Date longPeriodEnd = new Date();
    private Date shortPeriodStart = new Date();
    private Date shortPeriodEnd = new Date();
    private double profitLossPer = 1.0;
    private Date stopped = new Date();
	
	@Before
	public void setUp() throws Exception {
		stock = new Stock(1,"test");
	}

	@Test
	public void testTMAStrategyStockIntDateDateDateDate() {
		tmaStrategy = new TMAStrategy(stock, size,longPeriodStart, longPeriodEnd, shortPeriodStart, shortPeriodEnd);
		assert(tmaStrategy.getStock().getTicker().equals("test"));
		assert(tmaStrategy.getStock().getId() == 1);
		assert(tmaStrategy.getLongPeriodStart() == longPeriodStart);
		assert(tmaStrategy.getLongPeriodEnd() == longPeriodEnd);
		assert(tmaStrategy.getShortPeriodStart() == shortPeriodStart);
		assert(tmaStrategy.getShortPeriodEnd() == shortPeriodEnd);
	}

	@Test
	public void testTMAStrategyIntStockIntDoubleDoubleDateDateDateDateDoubleDate() {
		tmaStrategy = new TMAStrategy(id, stock, size, longAverage, shortAverage, longPeriodStart, 
				longPeriodEnd, shortPeriodStart, shortPeriodEnd, profitLossPer, stopped);
		assert(tmaStrategy.getStock().getTicker().equals("test"));
		assert(tmaStrategy.getStock().getId() == 1);
		assert(tmaStrategy.getLongPeriodStart() == longPeriodStart);
		assert(tmaStrategy.getLongPeriodEnd() == longPeriodEnd);
		assert(tmaStrategy.getShortPeriodStart() == shortPeriodStart);
		assert(tmaStrategy.getShortPeriodEnd() == shortPeriodEnd);
		assert(tmaStrategy.getSize() == 2);
		assert(tmaStrategy.getLongAverage() == 1.1);
		assert(tmaStrategy.getShortAverage() == 1.2);
		assert(tmaStrategy.getProfitLossPer()== 1.0);
	}

	@Test
	public void testGetId() {
		tmaStrategy = new TMAStrategy(id, stock, size, longAverage, shortAverage, longPeriodStart, 
				longPeriodEnd, shortPeriodStart, shortPeriodEnd, profitLossPer, stopped);
		assert(tmaStrategy.getId() == 1);
	}

	@Test
	public void testSetId() {
		tmaStrategy = new TMAStrategy(id, stock, size, longAverage, shortAverage, longPeriodStart, 
				longPeriodEnd, shortPeriodStart, shortPeriodEnd, profitLossPer, stopped);
		tmaStrategy.setId(10);
		assert(tmaStrategy.getId() == 10);
	}

	@Test
	public void testGetStock() {
		tmaStrategy = new TMAStrategy(id, stock, size, longAverage, shortAverage, longPeriodStart, 
				longPeriodEnd, shortPeriodStart, shortPeriodEnd, profitLossPer, stopped);
		assert(tmaStrategy.getStock().equals(stock));
	}

	@Test
	public void testSetStock() {
		
		tmaStrategy = new TMAStrategy(id, stock, size, longAverage, shortAverage, longPeriodStart, 
				longPeriodEnd, shortPeriodStart, shortPeriodEnd, profitLossPer, stopped);
		Stock stock1 = new Stock(2, "james");
		tmaStrategy.setStock(stock1);
		assert(tmaStrategy.getStock().equals(stock1));
	}

	@Test
	public void testGetSize() {
		tmaStrategy = new TMAStrategy(id, stock, size, longAverage, shortAverage, longPeriodStart, 
				longPeriodEnd, shortPeriodStart, shortPeriodEnd, profitLossPer, stopped);
		assert(tmaStrategy.getSize() == 2);
	}

	@Test
	public void testSetSize() {
		tmaStrategy = new TMAStrategy(id, stock, size, longAverage, shortAverage, longPeriodStart, 
				longPeriodEnd, shortPeriodStart, shortPeriodEnd, profitLossPer, stopped);
		tmaStrategy.setSize(3);
		assert(tmaStrategy.getSize() == 3);
	}

	@Test
	public void testGetLongAverage() {
		tmaStrategy = new TMAStrategy(id, stock, size, longAverage, shortAverage, longPeriodStart, 
				longPeriodEnd, shortPeriodStart, shortPeriodEnd, profitLossPer, stopped);
		assert(tmaStrategy.getLongAverage() == 1.1);
	}

	@Test
	public void testSetLongAverage() {
		tmaStrategy = new TMAStrategy(id, stock, size, longAverage, shortAverage, longPeriodStart, 
				longPeriodEnd, shortPeriodStart, shortPeriodEnd, profitLossPer, stopped);
		tmaStrategy.setLongAverage(2.22);
		assert(tmaStrategy.getLongAverage() == 2.22);
		
	}

	@Test
	public void testGetShortAverage() {
		tmaStrategy = new TMAStrategy(id, stock, size, longAverage, shortAverage, longPeriodStart, 
				longPeriodEnd, shortPeriodStart, shortPeriodEnd, profitLossPer, stopped);
		assert(tmaStrategy.getShortAverage() == 1.2);
	}

	@Test
	public void testSetShortAverage() {
		tmaStrategy = new TMAStrategy(id, stock, size, longAverage, shortAverage, longPeriodStart, 
				longPeriodEnd, shortPeriodStart, shortPeriodEnd, profitLossPer, stopped);
		tmaStrategy.setShortAverage(1.111);
		assert(tmaStrategy.getShortAverage() == 1.111);
	}

	@Test
	public void testGetLongPeriodStart() {
		tmaStrategy = new TMAStrategy(id, stock, size, longAverage, shortAverage, longPeriodStart, 
				longPeriodEnd, shortPeriodStart, shortPeriodEnd, profitLossPer, stopped);
		assert(tmaStrategy.getLongPeriodStart() == longPeriodStart);
	}

	@Test
	public void testSetLongPeriodStart() {
		tmaStrategy = new TMAStrategy(id, stock, size, longAverage, shortAverage, longPeriodStart, 
				longPeriodEnd, shortPeriodStart, shortPeriodEnd, profitLossPer, stopped);
		Date longPeriodStart1 = new Date();
		tmaStrategy.setLongPeriodStart(longPeriodStart1);
		assert(tmaStrategy.getLongPeriodStart() == longPeriodStart1);
	}

	@Test
	public void testGetLongPeriodEnd() {
		tmaStrategy = new TMAStrategy(id, stock, size, longAverage, shortAverage, longPeriodStart, 
				longPeriodEnd, shortPeriodStart, shortPeriodEnd, profitLossPer, stopped);
		assert(tmaStrategy.getLongPeriodEnd() == longPeriodEnd);
	}

	@Test
	public void testSetLongPeriodEnd() {
		tmaStrategy = new TMAStrategy(id, stock, size, longAverage, shortAverage, longPeriodStart, 
				longPeriodEnd, shortPeriodStart, shortPeriodEnd, profitLossPer, stopped);
		Date longPeriodEnd1 = new Date();
		tmaStrategy.setLongPeriodEnd(longPeriodEnd1);
		assert(tmaStrategy.getLongPeriodEnd() == longPeriodEnd1);
	}

	@Test
	public void testGetShortPeriodStart() {
		tmaStrategy = new TMAStrategy(id, stock, size, longAverage, shortAverage, longPeriodStart, 
				longPeriodEnd, shortPeriodStart, shortPeriodEnd, profitLossPer, stopped);
		assert(tmaStrategy.getShortPeriodStart() == shortPeriodStart);
	}

	@Test
	public void testSetShortPeriodStart() {
		tmaStrategy = new TMAStrategy(id, stock, size, longAverage, shortAverage, longPeriodStart, 
				longPeriodEnd, shortPeriodStart, shortPeriodEnd, profitLossPer, stopped);
		Date shortPeriodStart = new Date();
		tmaStrategy.setShortPeriodStart(shortPeriodStart);
		assert(tmaStrategy.getShortPeriodStart() == shortPeriodStart);
	}

	@Test
	public void testGetShortPeriodEnd() {
		tmaStrategy = new TMAStrategy(id, stock, size, longAverage, shortAverage, longPeriodStart, 
				longPeriodEnd, shortPeriodStart, shortPeriodEnd, profitLossPer, stopped);
		assert(tmaStrategy.getShortPeriodEnd() == shortPeriodEnd);
	}

	@Test
	public void testSetShortPeriodEnd() {
		tmaStrategy = new TMAStrategy(id, stock, size, longAverage, shortAverage, longPeriodStart, 
				longPeriodEnd, shortPeriodStart, shortPeriodEnd, profitLossPer, stopped);
		Date shortPeriodEnd1 = new Date();
		tmaStrategy.setShortPeriodEnd(shortPeriodEnd1);
		assert(tmaStrategy.getShortPeriodEnd() == shortPeriodEnd1);
	}

	@Test
	public void testGetProfitLossPer() {
		tmaStrategy = new TMAStrategy(id, stock, size, longAverage, shortAverage, longPeriodStart, 
				longPeriodEnd, shortPeriodStart, shortPeriodEnd, profitLossPer, stopped);
		assert(tmaStrategy.getProfitLossPer() == 1.0);
	}

	@Test
	public void testSetProfitLossPer() {
		tmaStrategy = new TMAStrategy(id, stock, size, longAverage, shortAverage, longPeriodStart, 
				longPeriodEnd, shortPeriodStart, shortPeriodEnd, profitLossPer, stopped);
		tmaStrategy.setProfitLossPer(3);
		assert(tmaStrategy.getProfitLossPer() == 3);
	}

	@Test
	public void testGetStopped() {
		tmaStrategy = new TMAStrategy(id, stock, size, longAverage, shortAverage, longPeriodStart, 
				longPeriodEnd, shortPeriodStart, shortPeriodEnd, profitLossPer, stopped);
		assert(tmaStrategy.getStopped() == stopped);
	}

	@Test
	public void testSetStopped() {
		tmaStrategy = new TMAStrategy(id, stock, size, longAverage, shortAverage, longPeriodStart, 
				longPeriodEnd, shortPeriodStart, shortPeriodEnd, profitLossPer, stopped);
		Date stopped1 = new Date();
		tmaStrategy.setStopped(stopped1);
		assert(tmaStrategy.getStopped() == stopped1);
	}

	@Test
	public void testToString() {
		String testString = new TMAStrategy(id, stock, size, longAverage, shortAverage, 
				longPeriodStart, longPeriodEnd, shortPeriodStart, shortPeriodEnd, profitLossPer, 
				stopped).toString();
		assert(testString.contains(new Integer(id).toString()));
		assert(testString.contains(new Stock(id, stock.getTicker()).toString()));
		assert(testString.contains(new Integer(size).toString()));
		assert(testString.contains(new Double(longAverage).toString()));
		assert(testString.contains(new Double(shortAverage).toString()));
		assert(testString.contains(new Date().toString()));
		assert(testString.contains(new Double(profitLossPer).toString()));
	}

}
